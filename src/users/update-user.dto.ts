// users/dto/update-user.dto.ts
import { IsString, IsOptional, IsEnum } from 'class-validator';

export class UpdateUserDto {
  @IsString()
  @IsOptional()
  readonly username?: string;

  @IsString()
  @IsOptional()
  readonly password?: string;

  @IsEnum(['Admin', 'TeamMember'])
  @IsOptional()
  readonly role?: string;
}
