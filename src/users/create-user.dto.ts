// users/dto/create-user.dto.ts
import { IsString, IsNotEmpty, IsEnum } from 'class-validator';

export class CreateUserDto {
  @IsString()
  @IsNotEmpty()
  readonly username: string;

  @IsString()
  @IsNotEmpty()
  readonly password: string;

  @IsEnum(['Admin', 'TeamMember'])
  readonly role: string;
}
