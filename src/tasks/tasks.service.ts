import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Task, TaskDocument } from './task.schema';
import { CreateTaskDto } from './create-task.dto';
import { UpdateTaskDto } from './update-task.dto';
import { NotificationsService } from '../notification/notification.service';
import { ActivityLogService } from '../activity-log/activity-log.service';
import { EventsGateway } from '../events/events.gateway';
import { Project, ProjectDocument } from '../projects/project.schema';


@Injectable()
export class TasksService {
  constructor(
    @InjectModel(Task.name) private taskModel: Model<TaskDocument>,
    @InjectModel(Project.name) private projectModel: Model<ProjectDocument>,
    private notificationsService: NotificationsService,
    private activityLogService: ActivityLogService,
    private eventsGateway: EventsGateway,
  ) {}

  async create(createTaskDto: CreateTaskDto): Promise<Task> {
    const createdTask:any = new this.taskModel(createTaskDto);
    await createdTask.save();

    // Add the new task to the project's tasks array
    const project = await this.projectModel.findById(createTaskDto.project).exec();
    if (!project) {
      throw new NotFoundException(`Project with ID ${createTaskDto.project} not found`);
    }
    project.tasks.push(createdTask?._id);
    await project.save();

    this.notificationsService.notifyUser(createTaskDto.assignedTo, `You have been assigned a new task: ${createTaskDto.name}`);
    this.activityLogService.logActivity(`Task ${createTaskDto.name} created`);
    this.eventsGateway.sendUpdate('taskCreated', createdTask);

    return createdTask;
    // const createdTask = new this.taskModel(createTaskDto);
    // await createdTask.save();
    // this.notificationsService.notifyUser(createTaskDto.assignedTo, `You have been assigned a new task: ${createTaskDto.name}`);
    // this.activityLogService.logActivity(`Task ${createTaskDto.name} created`);
    // this.eventsGateway.sendUpdate('taskCreated', createdTask);
    // return createdTask;
  }

  async findAll(): Promise<Task[]> {
    return this.taskModel.find().populate('project assignedTo').exec();
  }

  async findOne(id: string): Promise<Task> {
    const task = await this.taskModel.findById(id).populate('project assignedTo').exec();
    if (!task) {
      throw new NotFoundException(`Task with ID ${id} not found`);
    }
    return task;
  }

  async update(id: string, updateTaskDto: UpdateTaskDto): Promise<Task> {
    const existingTask = await this.taskModel.findByIdAndUpdate(id, updateTaskDto, { new: true }).exec();
    if (!existingTask) {
      throw new NotFoundException(`Task with ID ${id} not found`);
    }
    this.notificationsService.notifyUser(updateTaskDto.assignedTo, `Task ${updateTaskDto.name} updated`);
    this.activityLogService.logActivity(`Task ${updateTaskDto.name} updated`);
    this.eventsGateway.sendUpdate('taskUpdated', existingTask);
    return existingTask;
  }

  async remove(id: string): Promise<Task> {
    const deletedTask = await this.taskModel.findByIdAndDelete(id).exec();
    if (!deletedTask) {
      throw new NotFoundException(`Task with ID ${id} not found`);
    }
    this.activityLogService.logActivity(`Task ${id} deleted`);
    this.eventsGateway.sendUpdate('taskDeleted', deletedTask);
    return deletedTask;
  }

  async addComment(taskId: string, comment: string): Promise<Task> {
    const task = await this.taskModel.findById(taskId).exec();
    if (!task) {
      throw new NotFoundException(`Task with ID ${taskId} not found`);
    }
    task.comments.push(comment);
    await task.save();
    this.eventsGateway.sendUpdate('taskUpdated', task);
    return task;
  }

  async addAttachment(taskId: string, attachment: string): Promise<Task> {
    const task = await this.taskModel.findById(taskId).exec();
    if (!task) {
      throw new NotFoundException(`Task with ID ${taskId} not found`);
    }
    task.attachments.push(attachment);
    await task.save();
    this.eventsGateway.sendUpdate('taskUpdated', task);
    return task;
  }
}
