import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TasksService } from './tasks.service';
import { Task, TaskSchema } from './task.schema';
import { TasksController } from './tasks.controller';
import { NotificationsService } from 'src/notification/notification.service';
import { ActivityLogService } from 'src/activity-log/activity-log.service';
import { EventsGateway } from 'src/events/events.gateway';
import { Project, ProjectSchema } from '../projects/project.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Task.name, schema: TaskSchema }]),
    MongooseModule.forFeature([{ name: Project.name, schema: ProjectSchema }]),
  ],
  controllers: [TasksController],
  providers: [TasksService, NotificationsService, ActivityLogService, EventsGateway],
  exports: [TasksService],
})
export class TasksModule {}
