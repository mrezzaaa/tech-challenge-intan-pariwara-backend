import { IsString, IsDate, IsMongoId, IsOptional, IsArray } from 'class-validator';

export class CreateTaskDto {
  @IsString()
  readonly name: string;

  @IsString()
  readonly description: string;

  @IsMongoId()
  readonly project: string;

  @IsMongoId()
  readonly assignedTo: string;

  @IsDate()
  readonly dueDate: Date;

  @IsString()
  readonly status: string;

  @IsOptional()
  @IsArray()
  @IsString({ each: true })
  readonly comments?: string[];

  @IsOptional()
  @IsArray()
  @IsString({ each: true })
  readonly attachments?: string[];
}
