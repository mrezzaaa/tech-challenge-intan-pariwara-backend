import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Project } from 'src/projects/project.schema';
import { User } from 'src/users/users.schema';

export type TaskDocument = Task & Document;

@Schema()
export class Task {
  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  description: string;

  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'Project', required: true })
  project: string;

  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'User', required: true })
  assignedTo: string;

  @Prop({ required: true, default: Date.now })
  dueDate: Date;

  @Prop({ required: true, default: 'To Do' })
  status: string;

  @Prop([{ type: String }])
  comments: string[];

  @Prop([{ type: String }])
  attachments: string[];
}

export const TaskSchema = SchemaFactory.createForClass(Task);
