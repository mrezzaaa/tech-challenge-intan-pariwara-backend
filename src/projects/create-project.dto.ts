// projects/dto/create-project.dto.ts
import { IsString, IsNotEmpty, IsArray } from 'class-validator';
import { Types } from 'mongoose';

export class CreateProjectDto {
  @IsString()
  @IsNotEmpty()
  readonly name: string;

  @IsString()
  @IsNotEmpty()
  readonly description: string;

  @IsArray()
  readonly tasks?: Types.ObjectId[];

  @IsArray()
  readonly members?: Types.ObjectId[];
}
