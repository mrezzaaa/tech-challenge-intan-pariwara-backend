// projects/dto/update-project.dto.ts
import { IsString, IsOptional, IsArray } from 'class-validator';
import { Types } from 'mongoose';

export class UpdateProjectDto {
  @IsString()
  @IsOptional()
  readonly name?: string;

  @IsString()
  @IsOptional()
  readonly description?: string;

  @IsArray()
  @IsOptional()
  readonly tasks?: Types.ObjectId[];

  @IsArray()
  @IsOptional()
  readonly members?: Types.ObjectId[];
}
