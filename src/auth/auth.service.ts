// auth/auth.service.ts
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/users.service';
import { CreateUserDto } from 'src/users/create-user.dto';
import { User } from 'src/users/users.schema';
import * as bcrypt from 'bcryptjs';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async register(userDto: CreateUserDto): Promise<User> {
    const hashedPassword = await bcrypt.hash(userDto.password, 10);
    return this.usersService.create({ ...userDto, password: hashedPassword });
  }

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findByUsername(username);
    if (user && (await bcrypt.compare(pass, user.password))) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async login(user: any) {
    return this.validateUser(user.username,user.password).then((u)=>{
      if(u != null){
        const payload = { username: u?._doc?.username, sub: u?._doc?._id, role: u?._doc?.role };
        console.log("User:",u)
        return {
          access_token: this.jwtService.sign(payload),
        };
      }
      else{
        throw new UnauthorizedException();
      }

    })
  }
}
