import { Injectable } from '@nestjs/common';

@Injectable()
export class NotificationsService {
  notifyUser(userId: string, message: string) {
    // Implementation for notifying a user (e.g., email, in-app notification)
  }
}
